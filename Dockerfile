FROM nvidia/cuda:8.0-cudnn5-devel

ARG OPENPOSE_GPU_MODE=CUDA

RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install wget unzip lsof apt-utils lsb-core -y
RUN apt-get install libatlas-base-dev -y
RUN apt-get install libopencv-dev python-opencv python-pip -y   
RUN apt-get install git libgoogle-glog-dev -y

WORKDIR /app/openpose
RUN git clone https://github.com/CMU-Perceptual-Computing-Lab/openpose.git /app/openpose

RUN apt-get install -y sudo cmake

RUN sudo ln -sf /usr/lib/x86_64-linux-gnu/libhdf5_serial.so /usr/lib/x86_64-linux-gnu/libhdf5.so; \
	sudo ln -sf /usr/lib/x86_64-linux-gnu/libhdf5_serial_hl.so /usr/lib/x86_64-linux-gnu/libhdf5_hl.so

RUN bash ./ubuntu/install_caffe_and_openpose_JetsonTX2_JetPack3.1.sh; exit 0
#RUN apt-get install -y libboost.*dev  libprotobuf-dev;
RUN mkdir build; cd build; cmake -DGPU_MODE=${OPENPOSE_GPU_MODE} .. && make -j 8 && make -j 8 install
RUN cd models && ./getModels.sh && cd ..

